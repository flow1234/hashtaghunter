
    // Variables
    var tweetCount = 0;
    var tweetCountSelected = 0;
    var tweetMarkerDic = {};
    var tweetCluster;
    var selectedtweetArray = [];
    var startTweet;
    var jsonKeyArray = [];
    var heatMapDic = {};
    var currentHeatMapArray = [];
    var jsonData;
    var currentState = [];
    var jsonFile; //= "test3.json";
    var startYear; //= 2015;
    var startMonth;// = 11; // 11 = December
    var startDay;// = 01;
    var startHour;
    var startMinute;
    var stepper;
    var step;
    var timeStampSelected = false;
    var heatMap;
    var circleMarkers;
    var circleMarkerDic = {};
    var avgDistance = 0;

    var showGraph = false;
    var dataLoaded = false;
    var play = false;
    var updatingData = false;
    var animateInterval;

    $( "#slider" ).css('background', '#428BCA');
    $('.pie_progress').asPieProgress({
            namespace: 'pie_progress'
    });
    $("#hashtag").selectmenu({
        style:'dropdown',
        change: function( event, ui ) {setupHashtag();}
    });


        /* ======================================================================================================== */
        // Initialize Map

        // Nützliche Übersicht:
        // https://leanpub.com/leaflet-tips-and-tricks/read
        var mAttr = '#HashtagHunter'
            mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw';

        var grayscale = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mAttr}),
            streets   = L.tileLayer(mbUrl, {id: 'mapbox.streets', attribution: mAttr});
            watercolor = L.tileLayer('http://{s}.tile.stamen.com/watercolor/{z}/{x}/{y}.jpg', {attribution: mAttr});
            other = L.tileLayer('http://{s}.tile.cloudmade.com/1a1b06b230af4efdbb989ea99e9841af/998/256/{z}/{x}/{y}.png', {attribution: mAttr});

        var map = L.map('map', {
            center: [38.82259, -2.8125],
            zoom: 3,
            maxZoom: 16,
            minZoom: 3,
            zoomControl:false,
            layers: grayscale,
            fullscreenControl: true,
            fullscreenControlOptions: {position: 'topright'}
        });

        map.setMaxBounds([[-90, -180], [90, 180]]);

        // events are fired when entering or exiting fullscreen.
        map.on('enterFullscreen', function(){
            setTimeout(function () {
                drawChart();
            }, 500);
        });

        map.on('exitFullscreen', function(){
            setTimeout(function () {
                drawChart();
            }, 500);
        });

        var baseLayers = {
            "Grayscale": grayscale,
            "Streets": streets,
            "Watercolor" : watercolor,
            "Cloudmade": other
        };

        // Hab gesehn es gibt sauviele Layers:
        // https://www.drupal.org/project/leaflet_more_maps
        // http://flink.com.au/tips-tricks/27-reasons-not-to-use-google-maps
        heatMap = L.heatLayer([], {radius: 30, minOpacity:0.3});
        circleMarkers = new L.FeatureGroup();


        tweetCluster = new L.MarkerClusterGroup({
                    maxClusterRadius: 20,
                    //disableClusteringAtZoom: 14,
                    animate: true,
                    chunkedLoading: true,
                    chunkProgress: updateProgress,
                    iconCreateFunction: function(cluster) {
                    // get the number of items in the cluster
                    var count = cluster.getChildCount();

                    // figure out how many digits long the number is
                    var digits = (count+'').length;

                    // return a new L.DivIcon with our classes so we can
                    // style them with CSS. Take a look at the CSS in
                    // the <head> to see these styles. You have to set
                    // iconSize to null if you want to use CSS to set the
                    // width and height.
                        return new L.divIcon({
                          html: count,
                          className:'cluster digits-'+digits,
                          iconSize: null
                        });
                    }
         });
        map.addLayer(tweetCluster);

        var overlayMaps = {
            "Heatmap": heatMap,
            "Cluster": tweetCluster
           // "Markers": circleMarkers
        };
        L.control.layers(baseLayers, overlayMaps).addTo(map);

        /* ======================================================================================================== */
        // Info Box top Left
        var info = L.control({position:'topleft'});
        var selectmenu = L.control({position:'topleft'});

        info.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'info');
            this.update();
            return this._div;
        };

        info.update = function (date,time,tweetctr,tweetssec) {
            this._div.innerHTML = '<h4>Date: ' + (date ? date + '<br>': '<br>')+ '</h4>' +
                                  '<h4>Time: ' + (time ? time + '<br>': '<br>')+ '</h4><br>' +
                                  '<h5>Tweets: ' + (tweetctr ? tweetctr + '<br>': '<br>')+ '</h5>' + 
                                  '<h5>Tweets selected: ' + (tweetssec ? tweetssec + '<br>': '<br>')+ '</h5>'+
                                  '<h5>Avg Distance: ' + avgDistance +' km' + '</h5>';
                                 
        };

        selectmenu.onAdd = function (map) {
            this._div = L.DomUtil.create('div', 'selectmenu');
            this.update();
            return this._div;
        };

        selectmenu.update = function (date,time,tweetctr,tweetssec) {
            this._div.innerHTML =  '<select class="selectpicker show-tick" data-size="5" id="hashtag" onchange="setupHashtag()">'+
                                    '<option value="Choose hashtag...">Choose hashtag...</option>'+
                                    '<optgroup label="2016">' +
                                    '<option value="Brexit">#Brexit</option>'+
                                    '<option value="Ger">#DieMannschaft</option>'+
                                    '</optgroup>'+
                                    '<optgroup label="2014">' +
                                    '<option value="MTVEMA">#MTVEMA</option>'+
                                    '<option value="Beliebers">#Beliebers</option>'+
                                    '<option value="FollowMeCarter">#FollowMeCarter</option>'+
                                    '<option value="KCAMexico">#KCAMexico</option>'+
                                    '<option value="NeverForget">#NeverForget</option>'+
                                    '<option value="Something">#SomethingBigIsHappening</option>'+
                                    '<option value="foodporn">#foodporn</option>'+
                                    '</optgroup>'+
                                  '</select>';
        };

        info.addTo(map);
        


        // create sidebar for graph
        var sidebar = L.control.sidebar('sidebar', {
            closeButton: false,
            position: 'left',
            autoPan: false
        });
        map.addControl(sidebar);
        selectmenu.addTo(map);

function setupHashtag(){
  if(play){
    clearInterval(animateInterval);
    play = false;
  }

  jsonFile = $("#hashtag").val();
  if(jsonFile == "Choose hashtag..."){
      return;
  }
  else{
    console.log(jsonFile);
    loadJsonfile(jsonFile);
  }

}

function playTimeline(){

  if(!play && dataLoaded){
      animateInterval = setInterval(animateTimeline , 1000);
      play = true;
    }

}

function animateTimeline(){
 if (!updatingData){
      updatingData = true;
      console.log($("#slider").slider('value'));
      var currentValue = $("#slider").slider('value');
      var maxValue = $("#slider").slider("option", "max");
      var valueToSet = currentValue+stepper;

      if(valueToSet > maxValue){
         $("#slider").slider('value',startMinute);
      }
      else{
         $("#slider").slider('value',valueToSet);
      }
  }
  else{

  }

}

function stopTimeline(){
  if(play){
    clearInterval(animateInterval);
    play = false;
  }
  
  
}


        // Need webserver!?
        // http://oramind.com/country-border-highlighting-with-leaflet-js/
        // Formatiertes json File: "country-borders.json", sollte eig funktionieren...
function updateProgress(processed, total, elapsed, layersArray){

console.log("Progress"); {
      if (elapsed > 500) {

       if(!$('.pie_progress').is(":visible") && !play){
          $('.pie_progress').fadeIn( "slow", "linear" );
       }

        $('.pie_progress').asPieProgress('go',Math.round(processed/total*100) + '%');
      }

      if (processed === total) {
        if (!play) {
          $('.pie_progress').fadeOut( "slow", "linear" );
        };
        
        console.log("FINISHED");
        updatingData = false;
        
      }
    }

}



// Load JSON and Add TWEETS
function loadJsonfile(hashtagName){
   // d3.json(jsonFile, function(data){
                // console.log(data);


                  switch(hashtagName) {
                    case "MTVEMA":
                        jsonData = MTVEMA;
                        break;
                    case "Brexit":
                        jsonData = Brexit;
                        break;
                    case "Ger":
                        jsonData = Ger;
                        break;
                    case "JustinBieber":
                        jsonData = Justin;
                        break;
                    case "MPN":
                        jsonData = MPN;
                        break;
                    case "Beliebers":
                        jsonData = Beliebers;
                        break;
                    case "FollowMeCarter":
                        jsonData = FollowMeCarter;
                        break;
                    case "HealthCare":
                        jsonData = HealthCare;
                        break;
                    case "KCAMexico":
                        jsonData = KCAMexico;
                        break;
                    case "NeverForget":
                        jsonData = NeverForget;
                        break;
                    case "Something":
                        jsonData = Something;
                        break;
                    case "foodporn":
                        jsonData = foodporn;
                        break;
                    case "Ger2":
                        jsonData = Ger2;
                        break;
                    default:
                        return;
                  } 

                // set all jsonKeys
                jsonKeyArray = d3.keys(jsonData);

                //filter keys
                jsonKeyArray = $.grep(jsonKeyArray, function(item) { 
                      return item !== "setup" && item !== "startPoint";
                  });
                // sort keys
                jsonKeyArray.sort(function(a, b){
                  return convertKeyToDate(a)-convertKeyToDate(b)
                });

                // load setup form json
                if(jsonData["setup"]){
                 // console.log("load JSONFILE called ++++++++++++++++++");

                  tweetCount = 1;
                  tweetCountSelected = 0;
                  tweetCluster.clearLayers();
                  circleMarkers.clearLayers();
                  if(startTweet != null){
                   map.removeLayer(startTweet);
                  }
                  tweetMarkerDic = {};
                  selectedtweetArray = [];
                  heatMapDic = {};
                  currentHeatMapArray = [];
                  circleMarkerDic = {};


                  removeRowsFromChart(0); // clear chart data
                  currentState = []; //jsonKeyArray.slice(0);
                  lastKeyIndex = 0; //jsonKeyArray.length -1;

                   startYear = null; //= 2015;
                   startMonth = null;// = 11; // 11 = December
                   startDay = null;// = 01;
                   startHour = null;
                   startMinute = null;
                   stepper = null;
                   step = null;
                   range = null; //= 24; // = 2 Years
                   dataLoaded = false;

                  console.log("setup loaded");
                  startYear = jsonData["setup"]["startYear"];
                  startMonth = jsonData["setup"]["startMonth"]; //0-11
                  startDay = jsonData["setup"]["startDay"]; // 1-31
                  startHour = jsonData["setup"]["startHour"]; //0 -
                  startMinute = jsonData["setup"]["startMinute"]; // 0 - 
                  range = jsonData["setup"]["range"]; // in days
                  step = jsonData["setup"]["step"]; // minute, hour, day, week, month

                  if(jsonData["setup"]["avgdistance"]){
                      avgDistance = jsonData["setup"]["avgdistance"];
                      avgDistance = avgDistance.toFixed(2);
                  }
                  else{
                    avgDistance = 0;
                  }

                   var tempMinute = startMinute;
                   var tempHour = startHour;
                    if(startMinute < 10){
                        tempMinute = '0'+startMinute;
                    }
                    if(startHour < 10){
                        tempHour = '0'+startHour;
                    }


                // add StartPoint
                 var startPoint = jsonData["startPoint"];
                 var pinIcon = L.icon.pulse({iconSize:[14,14],color:'red'});
                 var tweettext = startPoint[0]["text"] + '<br>' +
                        '<p>' + tempHour+':'+tempMinute + " " + startDay+"-"+(startMonth+1)+"-"+startYear + '</p>';
                   
                 startTweet = L.marker( [startPoint[0]["latitude"], startPoint[0]["longitude"]], {icon: pinIcon} ).bindPopup(tweettext);
                 startTweet.setZIndexOffset(1000); 
                 map.addLayer(startTweet);

                  switch(step) {
                    case "minute":
                        stepper = 1;
                        break;
                    case "hour":
                        stepper = 60;
                        break;
                    case "day":
                        stepper = 60 * 24;
                        break;
                    case "week":
                        stepper = 60*24*7;
                        break;
                    case "month":
                        stepper = 60*24*30;
                        break;
                    default:
                        stepper = 1;
                  } 
                  var lastDateTemp = convertKeyToDate(jsonKeyArray[jsonKeyArray.length - 1]);
                  var startDateTemp = new Date(startYear, startMonth, startDay, startHour, startMinute, 0, 0);
                  var diff = Math.abs(lastDateTemp - startDateTemp);
                  sliderVal = startMinute + Math.ceil((diff / 1000 / 60));//startMinute + 60*24*range
                  console.log("sliderVal:" + sliderVal);
                  sliderDate = new Date(startYear, startMonth, startDay, startHour, sliderVal, 0, 0);
                  console.log("sliderDate:" + sliderDate);
                  setupSlider(sliderVal);
              }

              console.log("create markers started");
                  // load all markers on map
              var allmarkers = [];
              $.each(jsonKeyArray, function( index, key ) {

                           var myIcon = L.icon({
                          iconUrl: 'twitter_logo.png',
                          iconRetinaUrl:'twitter_logo.png',
                          iconSize: [25, 20],
                          iconAnchor: [9, 21],
                          popupAnchor: [14, -20]
                          });
                        

                
                var dataArray = jsonData[key];
                
                var keyDate = convertKeyToDate(key);
                var tempMinute = keyDate.getMinutes();
                var tempHour = keyDate.getHours();
                if(keyDate.getMinutes() < 10){
                    tempMinute = '0'+keyDate.getMinutes();
                }
                if(keyDate.getHours() < 10){
                    tempHour = '0'+keyDate.getHours();
                }

                var heatMapArray = [];
                var circleArray = [];
                var addMarkerArray = [];

                        $.each(dataArray, function( index2, value2 ) {
                             var popup = value2["text"] + '<br>' +
                                '<p>' + tempHour+':'+tempMinute + " " + keyDate.getDate()+"-"+(keyDate.getMonth()+1)+"-"+keyDate.getFullYear() + '</p>';
                            
                            // marker for clustering
                            var tweetmarker = L.marker( [value2["latitude"], value2["longitude"]], {icon: myIcon} ).bindPopup(popup);
                            addMarkerArray.push(tweetmarker);
                            allmarkers.push(tweetmarker);

                            // heatmap
                            heatMapArray.push([value2["latitude"], value2["longitude"]]);

                            // circle
                        /*    var circle = L.circle([value2["latitude"], value2["longitude"]], 20, {
                                color: 'red',
                                fillColor: '#f03',
                                fillOpacity: 0.5
                            }).bindPopup(popup);
                            circleArray.push(circle);*/
                        }); 
                        
                        // add to dictionaries
                        tweetMarkerDic[key] = addMarkerArray;
                        heatMapDic[key] = heatMapArray;  
                       // circleMarkerDic[key] = circleArray;


               /* $.each(circleMarkerDic[key], function( index, value ) {
                    circleMarkers.addLayer(value);
                });*/
                addDataToChart(convertKeyToDate(key),tweetMarkerDic[key].length);
                tweetCount = tweetCount + tweetMarkerDic[key].length;
              });// END FOR EACH

              console.log("create markers finished");
                
               currentState = jsonKeyArray.slice(0);
               currentHeatMapArray = [];
               $.each(currentState, function( index, value ) {
                 currentHeatMapArray = currentHeatMapArray.concat(heatMapDic[value]);
               });
              heatMap.setLatLngs(currentHeatMapArray);

              $('.pie_progress').asPieProgress('reset');
              console.log("allmarkers: " + allmarkers.length);
              tweetCluster.addLayers(allmarkers);

                // update Info
                var tempMinute = sliderDate.getMinutes();
                var tempHour = sliderDate.getHours();
                if(tempMinute < 10){
                  tempMinute = "0"+tempMinute;
                }
                if(tempHour < 10){
                  tempHour = "0"+tempHour;
                }
                info.update(''+sliderDate.getDate()+"-"+(sliderDate.getMonth()+1)+"-"+sliderDate.getFullYear(),tempHour + ":" + tempMinute,tweetCount,''+tweetCountSelected);
                dataLoaded = true;
             
  //  });
}



function addMarkersForKey(key){
     if(!tweetMarkerDic[key]){
        var myIcon = L.icon({
                  iconUrl: 'twitter_logo.png',
                  iconRetinaUrl:'twitter_logo.png',
                  iconSize: [25, 20],
                  iconAnchor: [9, 21],
                  popupAnchor: [14, -20]
        });
                
        var dataArray = jsonData[key];
        
        var keyDate = convertKeyToDate(key);
        var tempMinute = keyDate.getMinutes();
        var tempHour = keyDate.getHours();
        if(keyDate.getMinutes() < 10){
            tempMinute = '0'+keyDate.getMinutes();
        }
        if(keyDate.getHours() < 10){
            tempHour = '0'+keyDate.getHours();
        }

        var heatMapArray = [];
        var circleArray = [];
        var addMarkerArray = [];

                $.each(dataArray, function( index2, value2 ) {
                     var popup = value2["text"] + '<br>' +
                        '<p>' + tempHour+':'+tempMinute + " " + keyDate.getDate()+"-"+(keyDate.getMonth()+1)+"-"+keyDate.getFullYear() + '</p>';
                    
                    // marker for clustering
                    var tweetmarker = L.marker( [value2["latitude"], value2["longitude"]], {icon: myIcon} ).bindPopup(popup);
                    addMarkerArray.push(tweetmarker);

                    // heatmap
                    heatMapArray.push([value2["latitude"], value2["longitude"]]);

                    // circle
                    var circle = L.circle([value2["latitude"], value2["longitude"]], 20, {
                        color: 'red',
                        fillColor: '#f03',
                        fillOpacity: 0.5
                    }).bindPopup(popup);
                    circleArray.push(circle);
                }); 
                
                // add to dictionaries
                tweetMarkerDic[key] = addMarkerArray;
                heatMapDic[key] = heatMapArray;  
                circleMarkerDic[key] = circleArray;
         } 
   /* $.each(circleMarkerDic[key], function( index, value ) {
        circleMarkers.addLayer(value);
    });*/
    addDataToChart(convertKeyToDate(key),tweetMarkerDic[key].length);
    tweetCount = tweetCount + tweetMarkerDic[key].length;
    tweetCluster.addLayers(tweetMarkerDic[key]);
}
      
function removeMarkersForKey(key){
   /* $.each(circleMarkerDic[key], function( index, value ) {
        circleMarkers.removeLayer(value);
    });*/

   tweetCount = tweetCount - tweetMarkerDic[key].length;
   var removeMarkerArray = [];
   var removeMarkerArray = tweetMarkerDic[key];
   tweetCluster.removeLayers(removeMarkerArray);
   
}

//Setup Slider
function setupSlider(sliderValue){
   console.log("setupSlider called ++++++++++++++++++");
   // $("#date").val("Date: "+(sliderDate.getMonth()+1)+"/"+sliderDate.getFullYear());
    $("#slider").slider({
      value:sliderValue,
      min: startMinute,
      max: sliderValue, //startMinute+(range*24*60),
      step: stepper,
     // slide: function( event, ui ) {sliderChanged(event,ui);}
      change: function( event, ui ) {sliderChanged(event,ui);}
    });
}

// is called when slider value changes
function sliderChanged(event, ui){
  
    if(timeStampSelected){
       selectedtweetArray = [];
       tweetCountSelected = 0;

        // reset cluster
        tweetCluster.clearLayers();

        /* $.each(currentState, function( index, value ) {
             tweetCluster.addLayers(tweetMarkerDic[value] );
        });*/

        // reset heatmap
      /*  currentHeatMapArray = [];
        $.each(currentState, function( index, value ) {
            currentHeatMapArray = currentHeatMapArray.concat(heatMapDic[value]);
        });
        heatMap.setLatLngs(currentHeatMapArray);*/

        //reset circles
       /* circleMarkers.clearLayers();
        $.each(currentState, function( index, value ) {
             $.each(circleMarkerDic[value], function( index2, value2 ) {
                    circleMarkers.addLayer(value2);
              });
        });*/

        timeStampSelected = false;
    }

   if(dataLoaded){  // event.originalEvent
      console.log("sliderChanged called ++++++++++++++++++" +currentState.length);
      var currentStateArrayBefore = currentState.slice(0);

      sliderVal = ui.value;
      sliderDate = new Date(startYear, startMonth, startDay, startHour, ui.value, 0, 0); 

      if(currentState.length > 0){
            lastKeyIndex = currentState.length - 1;
        }
      else{
          lastKeyIndex = 0;
      }

      var lastKeyDate = convertKeyToDate(jsonKeyArray[lastKeyIndex]);

      if(sliderDate >= lastKeyDate){
        console.log("sliderdate >= lastKeyDate");
      for(var i = lastKeyIndex; i < jsonKeyArray.length; i++){
            var key = jsonKeyArray[i];
            var keyDate = convertKeyToDate(key);
            if(keyDate > sliderDate){
              break;
            }
            else{
              if($.inArray(key, currentState) > -1){
                    
              }
              else{
                currentState.push(key);
               // addMarkersForKey(key);
              }

            }
          } // END FOR
      } 
      else if (sliderDate < lastKeyDate){
        console.log("sliderdate < lastKeyDate");
          for(var i = lastKeyIndex; i >= 0; i--){
            var key = jsonKeyArray[i];
            var keyDate = convertKeyToDate(key);
            if(keyDate <= sliderDate){
              break;
            }
            else{
               if($.inArray(key, currentState) > -1){
                  //  removeMarkersForKey(key);
                }
              //filter keys
              currentState = $.grep(currentState, function(item) { 
                    return item !== key;
               });
            }
          } // END FOR

      }
      
      console.log("currentState " + currentState.length);

      if(!play){
        $('.pie_progress').asPieProgress('reset');
        if(map.hasLayer(tweetCluster)){
              $('.pie_progress').fadeIn( "slow", "linear" );
        }
      }

      var addToClusterArray = [];
      currentHeatMapArray = [];
      // update cluster
      if(currentStateArrayBefore.length < currentState.length){
        console.log(currentStateArrayBefore.length +"<" +currentState.length);
        for(var i = currentStateArrayBefore.length; i < currentState.length; i++){
          addToClusterArray = addToClusterArray.concat(tweetMarkerDic[currentState[i]]);
          addDataToChart(convertKeyToDate(currentState[i]),tweetMarkerDic[currentState[i]].length);
        }

         $.each(currentState, function( index, key ) {  
           currentHeatMapArray = currentHeatMapArray.concat(heatMapDic[key]);
         });
            //tweetcount
             tweetCount = tweetCount + addToClusterArray.length;
             // update tweetCluster
             tweetCluster.addLayers(addToClusterArray);
            // update heatmap
            heatMap.setLatLngs(currentHeatMapArray);
      }

      else if(currentStateArrayBefore.length > currentState.length){
          // clear chart
          removeRowsFromChart(0);
          tweetCluster.clearLayers();
           $.each(currentState, function( index, key ) {
             addToClusterArray = addToClusterArray.concat(tweetMarkerDic[key]);
             currentHeatMapArray = currentHeatMapArray.concat(heatMapDic[key]);
             addDataToChart(convertKeyToDate(key),tweetMarkerDic[key].length);
           });
            //tweetcount
            tweetCount = addToClusterArray.length;
            tweetCluster.addLayers(addToClusterArray);
            // update heatmap
            heatMap.setLatLngs(currentHeatMapArray);
      }
      else{
          // no new data
          updatingData = false;
      }


      

      if(!map.hasLayer(tweetCluster)){
          // no new data
          updatingData = false;
      }
     

      // update Info
      var tempMinute = sliderDate.getMinutes();
      var tempHour = sliderDate.getHours();
      if(tempMinute < 10){
        tempMinute = "0"+tempMinute;
      }
      if(tempHour < 10){
        tempHour = "0"+tempHour;
      }
      info.update(''+sliderDate.getDate()+"-"+(sliderDate.getMonth()+1)+"-"+sliderDate.getFullYear(),tempHour + ":" + tempMinute,tweetCount,''+tweetCountSelected);

   }
}

// Google Chart
 google.charts.load('current', {'packages':['corechart']});
 google.charts.setOnLoadCallback(initChart);
  var googleChart;
  var googleData;
  var lastSelectedKey;

  function chartSelectDataPoint(){
    console.log(googleChart.getSelection());

    if(googleChart.getSelection()[0]){

        if(selectedtweetArray.length == 0){
             //map.removeLayer(tweetCluster);
             //map.addLayer(selectedtweetCluster);
             tweetCluster.clearLayers();
             console.log("called once");
             timeStampSelected = true;
        }

     console.log("length: " + googleChart.getSelection().length);

      var currentSelectedKeys = [];
      $.each(googleChart.getSelection(), function( index, value ) {
                       var keyDate = googleData.getValue(value.row,0);
                       var key = convertDateToKey(keyDate);
                       currentSelectedKeys.push(key);
       });

            $('.pie_progress').asPieProgress('reset');
      
         if(selectedtweetArray.length < currentSelectedKeys.length){
             var key = currentSelectedKeys[currentSelectedKeys.length - 1];
             selectedtweetArray.push(key);
            // var tempMarkers = tweetMarkerDic[key];
             tweetCluster.addLayers(tweetMarkerDic[key] );
         }
         else{
                $.each(selectedtweetArray, function( index, value ) {
                        if($.inArray(value, currentSelectedKeys) < 0){
                            //var tempMarkers = tweetMarkerDic[value];
                            tweetCluster.removeLayers(tweetMarkerDic[value] );
                        }
                });

                // set selectedtweetArray = currentSelectedKeys
                    selectedtweetArray = currentSelectedKeys.slice(0);
         }

             console.log("selectedtweetArray: " + selectedtweetArray);

             // update heatmap
              currentHeatMapArray = [];
               $.each(selectedtweetArray, function( index, value ) {
                 currentHeatMapArray = currentHeatMapArray.concat(heatMapDic[value]);
               });
              heatMap.setLatLngs(currentHeatMapArray);

              //update circles
            /*  circleMarkers.clearLayers();
              $.each(selectedtweetArray, function( index, value ) {
                   $.each(circleMarkerDic[value], function( index2, value2 ) {
                    circleMarkers.addLayer(value2);
                });
              });*/

              tweetCountSelected = currentHeatMapArray.length;

              // update Info
                var tempMinute = sliderDate.getMinutes();
                var tempHour = sliderDate.getHours();
                if(tempMinute < 10){
                  tempMinute = "0"+tempMinute;
                }
                if(tempHour < 10){
                  tempHour = "0"+tempHour;
                }
                info.update(''+sliderDate.getDate()+"-"+(sliderDate.getMonth()+1)+"-"+sliderDate.getFullYear(),tempHour + ":" + tempMinute,tweetCount,''+tweetCountSelected);
              
          
    }
    else{
        selectedtweetArray = [];
        tweetCountSelected = 0;
        
        // update Info
        var tempMinute = sliderDate.getMinutes();
        var tempHour = sliderDate.getHours();
        if(tempMinute < 10){
          tempMinute = "0"+tempMinute;
        }
        if(tempHour < 10){
          tempHour = "0"+tempHour;
        }
        info.update(''+sliderDate.getDate()+"-"+(sliderDate.getMonth()+1)+"-"+sliderDate.getFullYear(),tempHour + ":" + tempMinute,tweetCount,''+tweetCountSelected);

        // reset cluster
        tweetCluster.clearLayers();
        $('.pie_progress').asPieProgress('reset');
        if(map.hasLayer(tweetCluster)){
              $('.pie_progress').fadeIn( "slow", "linear" );
        }

        var addToClusterArray = [];
        currentHeatMapArray = [];
         $.each(currentState, function( index, value ) {
             addToClusterArray = addToClusterArray.concat(tweetMarkerDic[value]);
             currentHeatMapArray = currentHeatMapArray.concat(heatMapDic[value]);
        });

        tweetCluster.addLayers(addToClusterArray);

        // reset heatmap
        heatMap.setLatLngs(currentHeatMapArray);

        //reset circles
     /*   circleMarkers.clearLayers();
        $.each(currentState, function( index, value ) {
             $.each(circleMarkerDic[value], function( index2, value2 ) {
                    circleMarkers.addLayer(value2);
              });
        });*/

        timeStampSelected = false;

    }

}

// init google chart
  function initChart(){
     googleChart = new google.visualization.LineChart(document.getElementById('curve_chart'));
     /*googleData = google.visualization.arrayToDataTable([
          ['Time', 'Tweets']
        ]);*/
    googleData = new google.visualization.DataTable();
    googleData.addColumn('datetime', 'Day');
    googleData.addColumn('number', '# of Tweets');

    google.visualization.events.addListener(googleChart, 'select', chartSelectDataPoint);
    drawChart();

    //googleChartLoaded = true;
    toggleMyGraph();
    // show sidebar
        setTimeout(function () {
            sidebar.show();
        }, 500);


    setupHashtag();

  }

// adds data to googleData
  function addDataToChart(date,value){
    googleData.addRow([date,value]);
    drawChart();

  }

  // remove data from googleData
  function removeRowsFromChart(startRow){

   var numberOfRows = googleData.getNumberOfRows();
   //console.log("Startrow:" +startRow + " NumberofRows: " + numberOfRows);
   //console.log("countRows: " + googleData.count) ;
   if(numberOfRows > startRow){
      googleData.removeRows(startRow, numberOfRows);
      drawChart();
   }

  }

// draw google chart
  function drawChart() {
    
       /* var data = google.visualization.arrayToDataTable([
          ['Time', 'Tweets', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);*/
      var time_pattern;
    if(step == "minute" || step == "hour"){
      time_pattern = "HH:mm, dd MMM yyyy";
    }
    else{
      time_pattern = "dd MMM, yyyy";
    }

     var date_formatter = new google.visualization.DateFormat({ 
     pattern: time_pattern
          }); 
     date_formatter.format(googleData, 0);

        var options = {
          title: '# of Tweets per timeframe',
          //curveType: 'function',
          legend: { position: 'bottom' },
          pointSize: 8,
          pointShape: 'square',
          selectionMode: 'multiple',
          vAxis: {minValue:0},
          animation: {
            duration: 400,
            easing: 'in'
          }

        };


        googleChart.draw(googleData, options);
  }

function toggleMyGraph(){

  console.log("toogle start");
  if(!showGraph){
     // $("#sidebar").stop();
      $( "#sidebar" ).animate({
            bottom: "-200px",
            height: "50px"
          }, 1000, function() {
            // Animation complete.
            $("#curve_chart").hide();
            showGraph = true;

        });

    }
    else{
     // $("#sidebar").stop();
      $( "#sidebar" ).animate({
            bottom: "0px",
            height: "250px"
          }, 1000, function() {
            // Animation complete.
            $("#curve_chart").show();
            drawChart();
            showGraph = false;
        });
    }

      console.log("toogle finished");


}

function convertKeyToDate(keyInput){
      // key:  D-MM-HH-DD-MM-YYYY 
      if(keyInput){
        var dateArray = keyInput.split("-");
        var keyDate = new Date(dateArray[5], dateArray[4]-1, dateArray[3], dateArray[2], dateArray[1], 0, 0);
        return keyDate;
      }
      
  }

  function convertDateToKey(dateInput){
        var keyYear = dateInput.getFullYear();
        var keyMonth = (dateInput.getMonth() + 1);
        var keyDay = dateInput.getDate();
        var keyHour = dateInput.getHours();
        var keyMinute = dateInput.getMinutes();
             
                  // 9 -> 09
                  if(keyMonth < 10){
                    keyMonth = "0"+keyMonth;
                  }
                  if(keyDay < 10){
                    keyDay = "0"+keyDay;
                  }
                  if(keyHour < 10){
                    keyHour = "0"+keyHour;
                  }
                  if(keyMinute < 10){
                    keyMinute = "0"+keyMinute;
                  }
                   // key:  D-MM-HH-DD-MM-YYYY 
                  return "D-"+keyMinute+"-"+keyHour+"-"+keyDay+"-"+keyMonth+"-"+keyYear;

  }

