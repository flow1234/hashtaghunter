import sqlite3 as lite
import sys
from lib import Tweet
import datetime
from math import sin, cos, sqrt, atan2, radians
import operator


def performQuery():
	print("Perform query")
	db = None
	
	try:
		chosenHashtags=[]
		
		variance=0
		print("Start connection.")
		db = lite.connect('C:/Users/Michael/Desktop/Seminar Data Science/database/Twitter.sqlite')
		print("Connected")
		cur = db.cursor()
		
		
		dayCount=0
		print("Day count is ",dayCount)
		print("Get number of days")
		cur.execute("SELECT date FROM tweets")
		dates = cur.fetchall()
		days=[]
		for date in dates:
			days.append(convertDate(date[0]))
		dayCount=len(set(days))
		print("Number of different days: ",dayCount)
		
		print("Start fetching hashtags:")
		cur.execute("SELECT hashtag, count FROM (SELECT hashtag, COUNT(*) AS `count` FROM hashtags GROUP BY hashtag ORDER BY count DESC) WHERE count>400")
		hashtags = cur.fetchall()
		print("Fetched")
		
		
		hashtagScore={}
		
		print("Getting scores of 100 most used hashtags:")
		for i in range(1000):
			tag=(hashtags[i][0])
			print("Hashtag ",i,": ")
			hashtagScore[tag]=0
			
			cur.execute("SELECT date FROM ((((SELECT hashtag, tweet FROM hashtags WHERE hashtag=?) AS tags) JOIN tweets ON tags.tweet=tweets.id))",[tag])
			occurences=cur.fetchall()
			print("Hashtag ",i," appears ",len(occurences)," times")
			
			print("Create dictionary for hashtag ",i)
			dateDict={}
			for tweet in occurences:
				arr=tweet[0].split(" ")
				shortD=arr[0]+"_"+arr[1]+" "+arr[2]
				if(shortD in dateDict):
					dateDict[shortD]+=1
				else:
					dateDict[shortD]=1
			
			print("Done creating dictionary for hashtag ",i, ". Its length is ",len(dateDict))
			sum=0
			sqSum=0
			
			print("Getting variance over each day for hashtag ",i)
			for key,val in dateDict.items():
				sum+=val
				sqSum+=(val*val)
			
			
			print("Get count of hashtag")
			cur.execute("SELECT COUNT(*) FROM hashtags WHERE hashtag=?",[tag])
			count=cur.fetchone()
			print("Total count is ",count[0])
			
			variance=(sqSum/dayCount)-((sum/dayCount)*(sum/dayCount))
			print("Variance of hashtag ",i," is: ", variance)
			
			if(variance<0):
				variance=(-variance)
			hashtagScore[(hashtags[i][0])] = variance
			print("Score of hashtag ",i," is: ",hashtagScore[hashtags[i][0]])
		
		print("Ranked all Hashtags.")
		print("Sort hashtags by variance:")
		sortedByScore = sorted(hashtagScore.items(), key=operator.itemgetter(1), reverse=True)
		print("Hashtags have been sorted.")
		print("Save tweets for 10 best scoring hashtags:")
		
		
		for i in range(10): #Nur die 10 am besten bewerteten Hashtags waehlen
			print("Getting all tweets of hashtag ",i)
			tagI =hashtags[i][0]
			cur.execute("SELECT date, latitude, longitude FROM (hashtags JOIN tweets ON hashtags.tweet=tweets.id) WHERE hashtag=?",(tagI,)) 
			tweets = cur.fetchall()
			
			print("Saving all tweets of hashtag ",i," to JSONFile")
			writeJsonFile(tagI,tweets, i, variance)
			print("Done with hashtag ",i)
		print("Finished program")
	except lite.Error as e:
		print ("Error %s:" % e.args[0])
		sys.exit(1)
	finally:
		if db:
			db.commit()
			db.close()

			
def getRange(start, end):
		
		s_min = int(start.split("-")[1])
		s_hou = int(start.split("-")[2])
		s_day = int(start.split("-")[3])
		s_mon = int(start.split("-")[4])
		s_yea = int(start.split("-")[5])
		
		s = datetime.datetime(s_yea,s_mon,s_day,s_hou,s_min)
		
		e_min = int(end.split("-")[1])
		e_hou = int(end.split("-")[2])
		e_day = int(end.split("-")[3])
		e_mon = int(end.split("-")[4])
		e_yea = int(end.split("-")[5])
		
		e = datetime.datetime(e_yea,e_mon,e_day,e_hou,e_min)
		
		output = str((((e - s).total_seconds()/60)/60/24))
		
		return output


def writeJsonFile(name, tweets, index, variance):

	print("Set up output")
	output= "{\"setup\":{"
	
	print("Get date")
	start = tweets[0]
	end=tweets[len(tweets)-1]
	duration = getRange((convertDate(start[0])), (convertDate(end[0])))
	
	startDate=convertDate(start[0]).split("-")
	startYear=float(startDate[5])
	startMonth=float(startDate[4])
	startDay=float(startDate[3])
	
	print("Get distances")
	avgDistance= getavaragedistance(tweets)
	
	strvariance=str(variance)
	if strvariance=="0":
		strvariance="None"

	output+="\"startYear\":"+str(startYear)+", \"startMonth\":"+str(startMonth)+", \"startDay\":"+str(startDay)+", \"startHour\":0.0"+", \"startMinute\":0.0"+", \"range\":"+str(duration)+", \"step\": \"day\", \"avgdistance\":"+str(avgDistance)+", \"dayvariance\":"+strvariance+"}"
	
	output+=", \"startPoint\":["
	
	differentDays = []
	print("Writing start point")
	output += "{\"latitude\":" + str(start[2]) + ",\"longitude\":" + str(start[1]) + ",\"text\":\"...\"}]"
	
	print("Getting different days")
	for tweet in tweets:
		differentDays.append(convertDate(tweet[0]))
	
	print("Appending all days")
	for day in set(differentDays):
		output += ",\"" + day + "\":["
		print(day)
		for i in range(len(tweets)):
			if convertDate(tweets[i][0]) == day:
				#print(str(i))
				output += "{\"latitude\":" + str(tweets[i][2]) + ",\"longitude\":" + str(tweets[i][1]) + ",\"text\":\"...\"},"
		output = output[:-1]
		output += "]"

	output += "}"
	
	print("Write to file")
	open("C:/Users/Michael/Desktop/Seminar Data Science/output/"+str(index)+"-"+name+".json", "w").write(output)

def getavaragedistance(tweets):
	n= len(tweets)
	R = 6373.0
	avgdistance=0
	samplesize = 15
	partitioncount=(int(n/1000))-1
	
	for p in range(0, partitioncount):
		partitionstart=p*1000
		for i in range(0,samplesize):
			x=partitionstart+i
			for y in range(int(x),(int(min(int(n),(int(partitionstart)+int(samplesize)))))):
				if x<y:
					lat1 = radians(tweets[x][2])
					lon1 = radians(tweets[x][1])
					lat2 = radians(tweets[y][2])
					lon2 = radians(tweets[y][1])
		
					dlon = lon2 - lon1
					dlat = lat2 - lat1

					a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
					c = 2 * atan2(sqrt(a), sqrt(1 - a))
			
					distance = R * c
					avgdistance+=((distance)/n)
	print("Got avarage distance")
	return avgdistance

	
def escapeText(text):
	return text.replace("\n"," ").replace("manuell", "man")
	
def convertDate(date):
	""" Format date to: D-MM-HH-DD-MM-YYYY """
	d = str(date).split(" ")

	output = "D-00-00-"+d[2]+"-"
	
	month = d[1]

	if month == 'Jan': 
		output += "01"
	elif month == 'Feb':
		output += "02"
	elif month == 'Mar':
		output += "03"
	elif month == 'Apr':
		output += "04"
	elif month == 'May':
		output += "05"
	elif month == 'Jun':
		output += "06"
	elif month == 'Jul':
		output += "07"
	elif month == 'Aug':
		output += "08"
	elif month == 'Sep':
		output += "09"
	elif month == 'Oct':
		output += "10"
	elif month == 'Nov':
		output += "11"
	elif month == 'Dec':
		output += "12"

	output += "-" + d[5]
	return output


def main():
	print("Startup")
	performQuery()


main()
