import json
import gzip
from lib import Tweet
import sqlite3 as lite
import sys
from os import listdir
from os.path import isfile, join, isdir
import datetime

def loadDocument(path):

	tweets = []
	print "Opening file " + path + "."
	
	with gzip.open(path, 'rb') as f:
		print "File " + path + " opened."
		for line in f:
			item = json.loads(line)
			
			if item['entities']['hashtags'] != []:
				print "Hashtags."
						
				if item['geo'] != None and item['geo']['coordinates']!=None:
					print "Geo."
					t_id = item['id']
					t_date = item['created_at']

					t_latitude = item['geo']['coordinates'][1]
					t_longitude = item['geo']['coordinates'][0]

					t_hashtags = []
					for hashtag in item['entities']['hashtags']:
						t_hashtags.append(hashtag['text'])
					t_text = item['text']
					print "Appending"
					tweets.append(Tweet.Data(t_id,t_date,t_latitude,t_longitude,t_hashtags,t_text))

				elif item['place']!=None and item['place']['bounding_box']!= None and item['place']['bounding_box']['coordinates'] != None and item['place']['bounding_box']['coordinates'] != []:
					t_id = item['id']
					t_date = item['created_at']

					t_latitude = ((item['place']['bounding_box']['coordinates'][0][0][0]+item['place']['bounding_box']['coordinates'][0][2][0])/2)
					t_longitude = ((item['place']['bounding_box']['coordinates'][0][0][1]+item['place']['bounding_box']['coordinates'][0][2][1])/2)

					t_hashtags = []
					for hashtag in item['entities']['hashtags']:
						t_hashtags.append(hashtag['text'])
					t_text = item['text']
					print "Appending"
					tweets.append(Tweet.Data(t_id,t_date,t_latitude,t_longitude,t_hashtags,t_text))		
	print "Writing to DB."
	return tweets


def addToDatabase(tweets):

	try:

		db = lite.connect('database/Twitter.sqlite')

		cur = db.cursor()

		for tweet in tweets:
				cur.execute("INSERT INTO tweets (id,date,latitude,longitude,text) VALUES (?,?,?,?,?)" , (tweet.id,tweet.date,tweet.position.latitude,tweet.position.longitude,tweet.text))	
				for hashtag in tweet.hashtags:
					cur.execute("INSERT INTO hashtags (tweet,hashtag) VALUES (?,?)" , (tweet.id,hashtag))

	except lite.Error, e:
		print "Error %s:" % e.args[0]
		sys.exit(1)

	finally:

		if db:

			db.commit()
			db.close()

def main():
	filepath=''
	if(len(sys.argv)>1):
		filepath=sys.argv[1]
	print "Reading from " + filepath
	startfolder=''	
	if(len(sys.argv)>2):
		startfolder=sys.argv[2]
	lastTime=''
	if(len(sys.argv)>3):
		lastTime=sys.argv[3]
	print "Starting at " + startfolder
	
	folders = [folder for folder in listdir(filepath) if isdir(join(filepath,folder))]
	for folder in folders:
		lookAtFolder=1
		if(folder>=startfolder):
			files = [f for f in listdir(join(filepath,folder)) if isfile(join(filepath,join(folder,f)))]
			for f in files:
				splitname=f.split(".")
				if(len(splitname)>2):
					if((folder>startfolder or splitname[0]>lastTime) and splitname[2] == "gz"):
						hasFoundEntryPoint=1
						addToDatabase(loadDocument(filepath+'/'+folder+'/'+f))
						print "Document", folder, "/", f, "imported"

main()
